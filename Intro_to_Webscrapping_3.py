


# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021


# Import library
import wikipedia

# Print wikipedia's attributes
print(dir(wikipedia))



# We collect the summary about "unbuntu" (linux distribution)
print(wikipedia.summary("unbuntu"))




# We use suggest on facebook
print(wikipedia.suggest("facebook"))


# We can try using facebook page directly
complete_content = wikipedia.page("facebook")
# We collect the page title
print(complete_content.title)
# We collect the page URL
print(complete_content.url)
# If we copy paste the given url we will be on facebook



# We collect the page content
print(complete_content.content)



# We retrieve an image of France from wikipedia
page_image = wikipedia.page("France")
print(page_image.images[0])
# It gives : https://upload.wikimedia.org/wikipedia/commons/a/ae/2013.07.05_roussillon_-_roque_anth%C3%A9ron_172.JPG
# We can copy and paste the given URI in our browser









################################################ Use of BeautifulSoup library 

# Import library
from bs4 import BeautifulSoup

#soup = BeautifulSoup(html_doc,'html.parser')

# We create a beautiful soup object using an html parser
# And we give text with html structure 
soup = BeautifulSoup('<b class="boldest">Extremely bold</b>','html.parser')


# We retrieve the b marker and print it
tag=soup.b
print(tag.name)

# We modify the tag.name and print it
tag.name = "blockquote"
print(tag.name)





################################## Example with an html document longer

html_doc = """
<html>
<head>
<title>The Dormouse's story</title>
</head>
<body>
<p class="my_class">
<b>The Dormouse's story</b>
</p>
</body>
</html>
"""

# We create a beautiful soup object with the previous html text
soup = BeautifulSoup(html_doc,'html.parser')


# We look at some markers and print them (header)
print(soup.head)
# body
print(soup.body)

# Paragraph p in the body
print(soup.body.p)

# marker b in paragraph p which is in the body
print(soup.body.p.b)

# We can have a look at the parent as well
print(soup.body.p.b.parent)

# We can have a look at the body content
print(soup.body.contents)

# We can look for children of the body
print(soup.body.findChild())




# Objet navigable string
# Rechercher des élements dans l"arbre 

# Create html document/text
html_doc = '<b class="boldest">Extremely bold</b>'

# Create object beautiful soup
soup = BeautifulSoup(html_doc,'html.parser')


# Find all classes that are "boldest"
tag = soup.find_all(class_='boldest')
print(tag)






######## Récupérer tous les liens URL d'une page wikipedia


################################################ Example to retrieve URLs from a wikipedia page

# Import library
import urllib.request

# Si fonctionne pas on peut faire la requête ssl

# If urllib.request doesn't work we can try with the following two lines ssl :
# import ssl
# ssl._create_default_https_context = ssl._create_unverified_context()

# Create request with the wikipedia page where we want to collect the links from
req = urllib.request.Request('http://en.wikipedia.org/wiki/Main_Page') 

try: # We load the page
    webpage=urllib.request.urlopen(req)
    soup=BeautifulSoup(webpage,'html.parser') # create beautiful soup object
    for anchor in soup.find_all('a'): # we look for all "a" markers
        print(anchor.get('href','/')) # in those "a" markers we look for href and /
        
except urllib.error.URLError as e: # if there is an error it prints it
    print("--->", e.reason)
    

# We collected all links that were on this wikipedia page
# Usually we collect information based on a marker, a class, or an id
# We have to look "by hand" where the information is, and then retrieve it with the corresponding id/marker/,...








########################################### Another example to collect weather data and display them in a tab


# Import libraries
import requests
import bs4
import pandas as pd


# We have to go to the website then right click to access the source code of the page
# In the body there are a lot of information  but we can use ctrl+f and look for specific words (for us today, tonight, ...)
# Thus, we see where the information that we want is contained (markers, id, ...) 


# We change the latitude and longitude coordinates
params = {"lat":42.9371, "lon":-75.6107}

# We connect to the URL
page = requests.get('http://forecast.weather.gov/MapClick.php', params)

# We create the beautiful soup object
soup = bs4.BeautifulSoup(page.content,'html.parser')

# We retrieve the information that we want based on its id
seven_day = soup.find(id="seven-day-forecast")  
seven_day


# We look for the class tombstone-container for example
forecast_items = seven_day.find_all(class_="tombstone-container")
# We store it and print it (for today)
today=forecast_items[0]
print(today.prettify()) 
# If we want tomorroz we have to change 0 by one, and so on



# We can look for the period-name in today object
period = today.find(class_="period-name").get_text()
period

# We can look for the short-desc in today object
short_desc = today.find(class_="short-desc").get_text()
short_desc

# We can llok for the temp in today object
temp = today.find(class_="temp").get_text()
temp





# We can use a for loop to collect all periods 
period_tags = seven_day.select(".tombstone-container .period-name")
periods = [pt.get_text() for pt in period_tags]
print(periods)



# We can also do it for short-desc, temp, img, ...
short_descs = [sd.get_text() for sd in seven_day.select(".tombstone-container .short-desc")]
temps =  [t.get_text() for t in seven_day.select(".tombstone-container .temp")]
descs = [d["title"] for d in seven_day.select(".tombstone-container img")]
print(short_descs)
print(temps)
print(descs)


# We can store the previous information in a dataframe
weather = pd.DataFrame({
    "period":periods,
    "short_desc":short_descs,
    "temp":temps,
    "desc":descs
    })

print(weather)

# We can transform the dataframe into an html file
html = weather.to_html()
print(html)



# We can put those information in a text file as well
text_file = open("index.html","w")
text_file.write(html)




############################# Conclusion 

# If the source code is modified, it's possible that our code won't work anymore, we'll need to adapt it 
# That's why it's better using API because it will always work 
# There is also a way using JSON
# It's important to create messages and notifications if it's not working

